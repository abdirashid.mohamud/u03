import sys
import os

def add_entry(note, guestbook):
    guestbook.append(note)
    save_guestbook(guestbook)

def list_entries(guestbook):
    for i, entry in enumerate(guestbook):
        print(f"{i+1}. {entry}")

def edit_entry(index, note, guestbook):
    if index > len(guestbook) or index < 1:
        print("Invalid index")
        return
    guestbook[-index] = note
    save_guestbook(guestbook)
 
def delete_entry(index, guestbook):
    if index > len(guestbook) or index < 1:
        print("Invalid index")
        return
    del guestbook[-index]
    save_guestbook(guestbook)

def save_guestbook(guestbook):
    with open('guestbook.txt', 'w') as f:
        f.writelines([entry+'\n' for entry in guestbook])

def load_guestbook(filename):
    try:
        with open(filename, 'r') as f:
            return f.read().splitlines()
    except FileNotFoundError:
        return []

if __name__ == "__main__":
    command = sys.argv[1]
    guestbook = load_guestbook('guestbook.txt')

    if command == "new":
        note = sys.argv[2]
        add_entry(note, guestbook)
    elif command == "list":
        list_entries(guestbook)
    elif command == "edit":
        index = int(sys.argv[2])
        note = sys.argv[3]
        edit_entry(index, note, guestbook)
    elif command == "delete":
        index = int(sys.argv[2])
        delete_entry(index, guestbook)
    else:
        print("Invalid command")

