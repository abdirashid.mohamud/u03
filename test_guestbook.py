import os
import pytest
from io import StringIO
from guestbook import add_entry, list_entries, edit_entry, delete_entry, load_guestbook, save_guestbook

class TestGuestbook:

    @pytest.fixture(autouse=True)
    def setup_method(self, tmp_path):
        # Create a test guestbook
        guestbook_file = tmp_path / 'test_guestbook.txt'
        with open(guestbook_file, 'w') as f:
            f.write('Entry 1\nEntry 2\nEntry 3\n')
        self.guestbook = load_guestbook(str(guestbook_file))

        yield

        # Remove the test guestbook file
        os.remove(str(guestbook_file))

    def test_add_entry(self):
        # Add a new entry to the guestbook
        add_entry('New entry', self.guestbook)
        assert self.guestbook[-1] == 'New entry'

    def test_list_entries(self, capsys):
        # Call the list_entries function
        list_entries(self.guestbook)

        # Capture the output and check that it matches the expected format
        captured = capsys.readouterr()
        expected_output = '1. Entry 1\n2. Entry 2\n3. Entry 3\n'
        assert captured.out == expected_output

    def test_edit_entry(self):
        # Edit the second entry in the guestbook
        edit_entry(2, 'Edited entry', self.guestbook)
        assert self.guestbook[1] == 'Edited entry'

    def delete_entry(index, guestbook):
        if index > len(guestbook) or index < 1:
            print("Invalid index")
            return
        del guestbook[len(guestbook) - index]
        save_guestbook(guestbook)




